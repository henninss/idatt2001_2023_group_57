package no.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for Player class
 */
class PlayerTest {

  /**
   * Tests the addHealth() method
   */
  @Test
  void addHealth() {
    Player player = new Player.Builder("player").health(40).build();
    player.addHealth(5);
    assertEquals(player.getHealth(), 45);
  }

  /**
   * Tests the addToInventory method
   */
  @Test
  void addToInventory() {
    List<String> inventory = new ArrayList<>();
    inventory.add("Axe");
    inventory.add("Potion");
    Player player = new Player.Builder("player").inventory(inventory).build();

    inventory.add("Sword");
    player.addToInventory("Sword");

    assertArrayEquals(player.getInventory().toArray(), inventory.toArray());
  }

  /**
   * Tests the builder for the Player class
   */
  @Test
  void build() {
    String name = "Player";
    int health = 100;
    int score = 300;
    int gold = 30;
    List<String> inventory = new ArrayList<>();
    inventory.add("Axe");
    inventory.add("Potion");

    Player player = new Player.Builder(name).health(health).score(score).gold(gold).inventory(inventory).build();

    assertEquals(player.getName(), name);
    assertEquals(player.getHealth(), health);
    assertEquals(player.getScore(), score);
    assertEquals(player.getGold(), gold);
    assertArrayEquals(player.getInventory().toArray(), inventory.toArray());
  }
}