package no.ntnu.idatt2001;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for the Story class
 */
class StoryTest {
  private Story story;
  private Passage secondPassage;
  private Passage thirdPassage;
  private Passage fourthPassage;

  /**
   * The setup before each test. Creates a Story-object with four passages.
   */
  @BeforeEach
  void setup() {
    Passage openingPassage = new Passage("A new beginning", "Once upon a time...");
    secondPassage = new Passage("Journey begins", "You find yourself in a forest...");
    thirdPassage = new Passage("The story continues", "You travel");
    fourthPassage = new Passage("The player tries to do something", "It works");

    Link openingLink = new Link("Journey begins", "Journey begins");

    Link thirdPassageLink = new Link("The story continues", "The story continues");
    assertTrue(openingPassage.addLink(openingLink));
    assertTrue(secondPassage.addLink(thirdPassageLink));


    story = new Story("Story", openingPassage);
    story.addPassage(secondPassage);
    story.addPassage(thirdPassage);
    story.addPassage(fourthPassage);

  }

  /**
   * Tests the removePassage() method. Runs the method to remove the fourthPassage in the story
   * and checks if the story then contains the fourthPassage. Then adds a link to the fourthPassage
   * and then adds the passage back to the story. Then tries to remove the passage again, and asserts
   * that the fourthPassage is still in the story.
   */
  @Test
  void removePassage() {
    Link fourthPassageLink = new Link("The player tries to do something", "The player tries to do something");
    secondPassage.addLink(fourthPassageLink);
    story.removePassage(fourthPassageLink);

    assertFalse(story.getPassages().contains(fourthPassage));

    Link newLink = new Link("Text", "Reference");
    assertTrue(fourthPassage.addLink(newLink));
    story.addPassage(fourthPassage);
    story.removePassage(fourthPassageLink);

    assertTrue(story.getPassages().contains(fourthPassage));
  }

  /**
   * Tests the getBrokenLinks() method by adding links that do not lead to any passages in the story.
   * Then checks if these links are returned by the getBrokenLinks()
   * method
   */
  @Test
  void getBrokenLinks() {
    Link brokenLink = new Link("Link1", "reference");
    Link brokenLink1 = new Link("Link2", "reference1");
    Link brokenLink2 = new Link("Link3", "reference2");

    assertTrue(secondPassage.addLink(brokenLink));
    assertTrue(secondPassage.addLink(brokenLink1));
    assertTrue(thirdPassage.addLink(brokenLink2));
    assertTrue(thirdPassage.addLink(brokenLink1));
    assertFalse(secondPassage.addLink(brokenLink));

    ArrayList<Link> brokenLinks = new ArrayList<>();
    brokenLinks.add(brokenLink);
    brokenLinks.add(brokenLink1);
    brokenLinks.add(brokenLink2);

    List<Link> brokenLinksResults = story.getBrokenLinks();

    assertTrue(brokenLinksResults.size() == brokenLinks.size() && brokenLinksResults.containsAll(brokenLinks) && brokenLinks.containsAll(brokenLinksResults));
  }

  /**
   * Tests the addPassage() method by adding a passage to the story and
   * checking if the story contains the passage.
   */
  @Test
  void addPassage() {
    Passage newPassage = new Passage("The new passage", "Something new");
    story.addPassage(newPassage);
    assertTrue(story.getPassages().contains(newPassage));
  }

  /**
   * Tests the getPassage() method by adding a passage to the story and
   * getting it by a link with the same reference.
   */
  @Test
  void getPassage() {
    Passage newPassage = new Passage("The new passage", "Something new");
    Link newLink = new Link("The new passage", "The new passage");
    story.addPassage(newPassage);
    assertEquals(story.getPassage(newLink), newPassage);

  }
}