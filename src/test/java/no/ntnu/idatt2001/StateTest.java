package no.ntnu.idatt2001;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the State class.
 */
public class StateTest {
  private Game game;
  private Passage firstPassage;

  /**
   * The setup before each test. Creates a Game object with a Story. This makes the player start
   * out with 100 health and 0 gold.
   */
  @BeforeEach
  void setup() throws IOException {
    Story story = PathsFileController.readStory(Objects.requireNonNull(getClass().getClassLoader()
        .getResource("stories/prisonbreak.paths")).getPath());
    Player player = new Player.Builder("Test").build();
    game = new Game(player, story, new ArrayList<>());
    firstPassage = game.begin();
  }

  /**
   * Tests the undo method by checking if it throws an exception when trying to undo from the first
   * passage.
   */
  @Test
  void testUndoFromFirstPassage() {
    // Direct method reference to the undo method
    assertThrows(NoSuchElementException.class, game::undo);
  }

  /**
   * Tests the undo method by checking if it returns the first passage when undoing from the second
   * passage.
   */
  @Test
  void testUndoFromSecondPassage() {
    game.go(firstPassage.getLinks().get(0));
    Passage undoToFirstPassage = game.undo();
    assertNotNull(undoToFirstPassage);
  }
}
