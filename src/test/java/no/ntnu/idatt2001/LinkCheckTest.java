package no.ntnu.idatt2001;

import no.ntnu.idatt2001.actions.GoldAction;
import no.ntnu.idatt2001.actions.HealthAction;
import no.ntnu.idatt2001.actions.ScoreAction;
import no.ntnu.idatt2001.goals.GoldGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for the LinkCheck class
 */
class LinkCheckTest {
  private LinkCheck linkCheck;
  private Player player;


  /**
   * The setup that runs before each test.
   * Builds a LinkCheck object and a Player object
   */
  @BeforeEach
  public void setup() {
    GoldGoal goal = new GoldGoal(20, 100);
    linkCheck = new LinkCheck("AlternatePassage", goal);
    HealthAction healthAction = new HealthAction(10);
    ScoreAction scoreAction = new ScoreAction(10);
    linkCheck.addAction(healthAction);
    linkCheck.addAction(scoreAction);

    player = new Player.Builder("Player").gold(20).build();
  }

  /**
   * Tests the addAction() method
   */
  @Test
  void addAction() {
    GoldAction goldAction = new GoldAction(20);
    linkCheck.addAction(goldAction);
    assertEquals(linkCheck.getActions().get(2), goldAction);
  }

  /**
   * Tests the checkLinkGoal() method
   */
  @Test
  void checkLinkGoal() {
    Player player2 = new Player.Builder("Player2").gold(10).build();

    assertTrue(linkCheck.checkLinkGoal(player));
    assertFalse(linkCheck.checkLinkGoal(player2));

  }

  /**
   * Tests the executeActions() method by executing the actions on a player
   */
  @Test
  void executeActions() {
    linkCheck.executeActions(player);

    assertEquals(player.getHealth(), 110);
    assertEquals(player.getScore(), 10);

  }
}