package no.ntnu.idatt2001;

import no.ntnu.idatt2001.actions.GoldAction;
import no.ntnu.idatt2001.goals.ScoreGoal;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Unit test for the Link class.
 *
 */
public class LinkTest {
  private final Link link = new Link("Test tunnel", "tunnel");

  /**
   * Checks if the toString() method actually returns a String object
   */
  @Test
  public void toStringReturnsString() {
    assertNotNull(link.toString());
  }

  /**
   * Tests the addAction() method
   */
  @Test
  void addAction() {
    GoldAction goldAction = new GoldAction(20);
    link.addAction(goldAction);

    assertEquals(link.getActions().get(0), goldAction);
  }

  /**
   * Tests the addLinkCheck() method
   */
  @Test
  void addLinkCheck() {
    ScoreGoal scoreGoal = new ScoreGoal(20, 1);
    LinkCheck linkCheck = new LinkCheck("alt", scoreGoal);

    link.addLinkCheck(linkCheck);

    assertEquals(link.getLinkChecks().get(0), linkCheck);
  }

  /**
   * Tests the executeLink() method by executing a link on a Player-object and
   * checking if the Link's gold action was successfully executed on the Player.
   */
  @Test
  void executeLink() {
    Player player = new Player.Builder("player1").build();
    GoldAction goldAction = new GoldAction(20);
    link.addAction(goldAction);

    link.executeLink(player);

    assertEquals(player.getGold(), 20);
  }

  /**
   * Tests if the executeLink() method successfully changes reference
   * when it is executed on a Player who triggers a LinkCheck.
   */
  @Test
  void executeLinkWhenTriggeringLinkCheck() {
    Player playerWhoTriggersLinkCheck = new Player.Builder("player").score(20).build();
    ScoreGoal scoreGoal = new ScoreGoal(20, 1);
    LinkCheck linkCheck = new LinkCheck("alt", scoreGoal);
    link.addLinkCheck(linkCheck);

    link.executeLink(playerWhoTriggersLinkCheck);

    assertEquals(link.getReference(), "alt");
  }


}
