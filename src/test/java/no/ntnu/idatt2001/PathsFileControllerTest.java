package no.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the PathsFileController class.
 */
class PathsFileControllerTest {
  /**
   * Tests the loadFile method by checking if the title and number of passages are correct.
   *
   * @throws IOException if the file loading fails
   */
  @Test
  void loadFile() throws IOException {
    Story story = PathsFileController.readStory(Objects.requireNonNull(getClass().getClassLoader()
        .getResource("stories/prisonbreak.paths")).getPath());
    assertEquals("Prison Break", story.getTitle());
    assertEquals(56, story.getPassages().size());
  }

  /**
   * Tests the loadFile method by checking if it throws an exception when trying to load a file that
   * is badly formatted.
   */
  @Test
  void loadBadFile() {
    assertThrows(IOException.class, () ->
        PathsFileController.readStory(Objects.requireNonNull(getClass().getClassLoader()
            .getResource("stories/bad_format.paths")).getPath()));
  }
}