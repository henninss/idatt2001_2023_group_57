package no.ntnu.idatt2001;

import no.ntnu.idatt2001.goals.GoldGoal;
import no.ntnu.idatt2001.goals.HealthGoal;
import no.ntnu.idatt2001.goals.InventoryGoal;
import no.ntnu.idatt2001.goals.ScoreGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit test for classes that implement the Goal-interface
 */
class GoalTest {
  private Player player;
  private Player player2;

  /**
   * The setup that runs before each unit test. Builds two Player objects.
   */
  @BeforeEach
  public void setup() {
    List<String> inventory = new ArrayList<>();
    inventory.add("Potion");
    inventory.add("Sword");

    List<String> inventory2 = new ArrayList<>();
    inventory2.add("Axe");
    inventory2.add("Potion");

    player = new Player.Builder("Player").health(20).score(100).gold(30).inventory(inventory)
        .build();

    player2 = new Player.Builder("Player2").health(5).score(20).gold(10).inventory(inventory2)
        .build();


  }

  /**
   * Tests the GoldGoal class
   */
  @Test
  public void GoldGoal() {
    GoldGoal goldGoal = new GoldGoal(20, 100);

    assertTrue(goldGoal.isFulfilled(player));
    assertFalse(goldGoal.isFulfilled(player2));
  }

  /**
   * Tests the HealthGoal class
   */
  @Test
  public void HealthGoal() {
    HealthGoal healthGoal = new HealthGoal(10, 100);

    assertTrue(healthGoal.isFulfilled(player));
    assertFalse(healthGoal.isFulfilled(player2));
  }

  /**
   * Tests the InventoryGoal class.
   */
  @Test
  public void InventoryGoal() {
    InventoryGoal inventoryGoal = new InventoryGoal(Arrays.asList("Sword", "Potion"), 100);

    assertTrue(inventoryGoal.isFulfilled(player));
    assertFalse(inventoryGoal.isFulfilled(player2));
  }

  /**
   * Tests the ScoreGoal class
   */
  @Test
  public void ScoreGoal() {
    ScoreGoal scoreGoal = new ScoreGoal(90, 100);

    assertTrue(scoreGoal.isFulfilled(player));
    assertFalse(scoreGoal.isFulfilled(player2));
  }
}