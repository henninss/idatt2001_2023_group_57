package no.ntnu.idatt2001;

import no.ntnu.idatt2001.actions.GoldAction;
import no.ntnu.idatt2001.actions.HealthAction;
import no.ntnu.idatt2001.actions.InventoryAction;
import no.ntnu.idatt2001.actions.ScoreAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for classes that implement the Action-interface
 */
class ActionTest {

  private Player player;

  /**
   * The setup that runs before each unit test. Builds a Player object.
   */
  @BeforeEach
  public void setup() {
    List<String> inventory = new ArrayList<>();
    inventory.add("Axe");
    inventory.add("Potion");

    player = new Player.Builder("Player").health(20).score(100).gold(30).inventory(inventory).build();
  }

  /**
   * Tests the GoldAction class
   */
  @Test
  public void GoldAction() {
    GoldAction goldAction = new GoldAction(20);

    goldAction.execute(player);

    assertEquals(player.getGold(), 50);
  }

  /**
   * Tests the HealthAction class
   */
  @Test
  public void HealthAction() {
    HealthAction healthAction = new HealthAction(20);

    healthAction.execute(player);

    assertEquals(player.getHealth(), 40);
  }

  /**
   * Tests the InventoryAction class by adding the String
   * "Sword" to player inventory.
   */
  @Test
  public void InventoryActionAddingAnItem() {
    InventoryAction inventoryAction = new InventoryAction("Sword");

    inventoryAction.execute(player);

    assertTrue(player.getInventory().contains("Sword"));
  }

  /**
   * Tests the InventoryAction by removing the String "Axe"
   * from player inventory.
   */
  @Test
  public void InventoryActionRemovingAnItem() {
    InventoryAction removeAction = new InventoryAction("-Axe");

    removeAction.execute(player);

    assertFalse(player.getInventory().contains("Axe"));
  }


  /**
   * Tests the ScoreAction class
   */
  @Test
  public void ScoreAction() {
    ScoreAction scoreAction = new ScoreAction(20);

    scoreAction.execute(player);

    assertEquals(player.getScore(), 120);
  }

}