package no.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Objects;

/**
 * Tests the SaveFileController class.
 */
public class SaveFileTest {
  /**
   * Tests the loadSave method by checking if the save loads.
   *
   * @throws IOException if the file loading fails.
   */
  @Test
  void loadSave() throws IOException {
    Story story = PathsFileController.readStory(Objects.requireNonNull(getClass().getClassLoader()
        .getResource("stories/prisonbreak.paths")).getPath());
    SaveFileController.readSave(Objects.requireNonNull(getClass().getClassLoader()
        .getResource("saves/test.save")).getPath(), story);
  }
}
