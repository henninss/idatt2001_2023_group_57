package no.ntnu.idatt2001;

import no.ntnu.idatt2001.actions.GoldAction;
import no.ntnu.idatt2001.actions.HealthAction;
import no.ntnu.idatt2001.goals.ScoreGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for Game class
 */
class GameTest {
  private Game game;
  private Passage passage1;
  private Passage passage2;
  private Passage passage3;
  private Passage passage4;
  private Link link1;
  private Link link2;

  /**
   * The setup before each test.
   * Creates a Game object with a Story with two passages.
   * This makes the player start out with 100 health and 0 gold.
   */
  @BeforeEach
  void setup() {
    Player player = new Player.Builder("player").build();

    passage1 = new Passage("OpeningPassage", "");
    passage2 = new Passage("SecondPassage", "");
    passage3 = new Passage("ThirdPassage", "");
    passage4 = new Passage("AlternatePassage", "");
    link1 = new Link("Link", "SecondPassage");
    link2 = new Link("Link", "ThirdPassage");

    GoldAction goldAction = new GoldAction(50);
    HealthAction healthAction = new HealthAction(20);
    link1.addAction(goldAction);
    link2.addAction(healthAction);

    link2.addLinkCheck(new LinkCheck("AlternatePassage", new ScoreGoal(20)));

    assertTrue(passage1.addLink(link1));
    assertTrue(passage2.addLink(link2));

    Story story = new Story("Story", passage1);
    story.addPassage(passage1);
    story.addPassage(passage2);
    story.addPassage(passage3);
    story.addPassage(passage4);
    game = new Game(player, story, null);
  }

  /**
   * Tests the .begin() method by checking if it returns
   * the opening passage in the story
   */
  @Test
  void begin() {
    assertEquals(game.begin(), passage1);
  }

  /**
   * Tests the go() method by checking if it returns
   * the passage in  the story correlating with the link.
   * Checks if the links actions were executed on the player.
   */
  @Test
  void go() {
    assertEquals(game.go(link1), passage2);
    assertEquals(game.getPlayer().getGold(), 50);
  }

  /**
   * Tests the go() method when the Player has the attribute
   * to trigger a LinkCheck, which should change the Passage
   * that is returned.
   */
  @Test
  void goAndTriggerLinkCheck() {
    game.getPlayer().setScore(20);

    assertEquals(game.go(link2), passage4);
  }



  /**
   * Tests the undo() method by making the game go through
   * link1 which increases the player's gold by 50 and link2 which
   * increases the player's health by 40. Then uses the undo method
   * to check if it returns the previous passage correctly and also that
   * the attributes of player gets rolled back correctly.
   */
  @Test
  void undo() {
        game.begin();
        game.go(link1);
        game.go(link2);

        assertEquals(game.getPlayer().getHealth(), 120);
        assertEquals(game.getPlayer().getGold(), 50);

        assertEquals(game.undo(),  passage2);
        assertEquals(game.getPlayer().getHealth(), 100);

        assertEquals(game.undo(),  passage1);

        game.go(link1);
        assertEquals(game.getPlayer().getGold(), 50);

        assertEquals(game.undo(),  passage1);
        assertEquals(game.getPlayer().getGold(), 0);
  }

  /**
   * Tests if the undo() method works when triggering linkChecks.
   */
  @Test
  void undoWhenTriggeringLinkChecks() {
    game.getPlayer().setScore(20);
    game.go(link1);
    assertEquals(game.go(link2), passage4);
    assertEquals(game.undo(), passage2);
    game.getPlayer().setScore(10);
    assertEquals(game.go(link2), passage3);
  }

  /**
   * Tests the hasGameStates() method.
   */
  @Test
  void hasGameStates() {
    game.begin();
    game.go(link1);
    assertTrue(game.hasOtherGameStates());
    game.undo();
    assertFalse(game.hasOtherGameStates());
  }


}