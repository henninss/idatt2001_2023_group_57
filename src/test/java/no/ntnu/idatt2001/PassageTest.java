package no.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for the Passage class. AddLink method is tested in StoryTest and not here.
 */
public class PassageTest {
  private final Passage passage1 = new Passage("Opening", "Once upon a time");
  private final Passage passage2 = new Passage("Opening", "Once upon a time");

  /**
   * Check if the toString() method actually returns a String object
   */
  @Test
  public void toStringReturnsString() {
    assertNotNull(passage1.toString());
  }

  @Test
  public void objectsAreEqual() {
    assertEquals(passage1, passage2);
  }

  /**
   * Tests the hasLinks method
   */
  @Test
  void hasLinks() {
    Link link = new Link("", "");
    assertTrue(passage2.addLink(link));

    assertFalse(passage1.hasLinks());
    assertTrue(passage2.hasLinks());
  }

  /**
   * Tests the addLink() method. Checks first if the link was successfully added,
   * and then checks if the method returns false if one tries to add the same
   * link twice.
   */
  @Test
  void addLink() {
    Link link = new Link("", "");

    passage1.addLink(link);

    assertTrue(passage1.getLinks().contains(link));

    assertFalse(passage1.addLink(link));
  }
}
