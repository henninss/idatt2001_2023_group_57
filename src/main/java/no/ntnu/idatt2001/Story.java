package no.ntnu.idatt2001;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The Story class represents a narrative with passages and links.
 */
public class Story {
  private final String title;
  private final Map<Link, Passage> passages = new HashMap<>();
  private Passage openingPassage;

  /**
   * Constructor for Story.
   *
   * @param title          The title of the story
   * @param openingPassage The first Passage of the story
   */
  Story(String title, Passage openingPassage) {
    this.title = title;
    this.openingPassage = openingPassage;
  }

  /**
   * Returns the title of the Story.
   *
   * @return title
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the opening-passage of the story.
   *
   * @return openingPassage
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }

  /**
   * Set-method for the opening passage of the story.
   *
   * @param passage Is the new opening passage
   */
  public void setOpeningPassage(Passage passage) {
    this.openingPassage = passage;
  }

  /**
   * Adds a passage to the story with a link that has the
   * same title and reference to the Passage-object's title.
   *
   * @param passage Is the Passage-object added to the story.
   */
  public void addPassage(Passage passage) {
    Link link = new Link(passage.getTitle(), passage.getTitle());
    passages.put(link, passage);
  }

  /**
   * Returns the passage connected with the given link.
   *
   * @param link The link of the passage to retrieve
   * @return The passage connected to the link
   * @throws NullPointerException if the link does not exist in
   *                              the HashMap "passages"
   */
  public Passage getPassage(Link link) throws NullPointerException {
    return passages.get(link);
  }

  /**
   * Get method for a collection of all passages in the story.
   *
   * @return a collection of passages.
   */
  public Collection<Passage> getPassages() {
    return passages.values();
  }

  /**
   * Removes the passage correlating with the
   * link given as parameter from the hashmap 'passages',
   * as long as the passage does not have any other links.
   *
   * @param link of the passage that is to be removed.
   */
  public void removePassage(Link link) throws NullPointerException {
    if (!passages.get(link).hasLinks()) {
      passages.remove(link);
    }
  }

  /**
   * Method that gets all the links that does not correlate with
   * a Passage-object in the Hashmap 'passages'.
   * Note: does not currently find broken links in the opening passage of the story.
   *
   * @return a list of the broken links.
   */
  public List<Link> getBrokenLinks() {
    return passages.values().stream().map(Passage::getLinks).toList()
        .stream().flatMap(List::stream).toList()
        .stream().filter(link ->
            !passages.containsKey(link)).distinct().collect(Collectors.toList());
  }

  /**
   * Tests if an object contains equal values to another object.
   *
   * @param o Is the object that gets compared
   * @return true or false depending on if the objects are equal or not.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (this.getClass() != o.getClass()) {
      return false;
    }
    Story story = (Story) o;
    return title.equals(story.getTitle())
        && passages.values().equals(story.getPassages())
        && openingPassage.equals(story.getOpeningPassage());
  }

  /**
   * Computes the hash code of the object.
   *
   * @return the hash code value
   */
  @Override
  public int hashCode() {
    return Objects.hash(title, passages, openingPassage);
  }

}
