package no.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.List;
import no.ntnu.idatt2001.actions.Action;

/**
 * A Link makes it possible to traverse Passages. It is what binds together the components of
 * a Story. A Link includes a list of Action-objects in a list called "actions". It has also a
 * set of LinkCheck-objects in a list called "linkChecks".
 */
public class Link {
  private final String text;
  private final String originalReference;
  private final List<Action> actions;
  private final List<LinkCheck> linkChecks;
  private String reference;

  /**
   * Constructor for a Link. Sets the originalReference attribute to the reference given in the
   * parameter. Also sets the lists "actions" and "linkChecks" to new ArrayLists.
   *
   * @param text      A descriptive text indicating a choice or an event in the story. This is what
   *                  is visible to the player
   * @param reference A string that unambiguously refers to a Passage in the story. This is what is
   *                  in practice the title of the Passage you want the Link to refer to
   */
  Link(String text, String reference) {
    this.text = text;
    this.reference = reference;
    originalReference = reference;
    this.actions = new ArrayList<>();
    this.linkChecks = new ArrayList<>();
  }

  /**
   * Returns the text of the Link.
   *
   * @return The text associated with the Link
   */
  public String getText() {
    return text;
  }

  /**
   * Returns the reference to the Passage the links refers to.
   *
   * @return The associated Passage as a string
   */
  public String getReference() {
    return reference;
  }

  /**
   * Set method for the reference of the Link.
   *
   * @param reference Is the new reference.
   */
  public void setReference(String reference) {
    this.reference = reference;
  }

  /**
   * Adds an Action-object to the list called "actions".
   *
   * @param action Is the Action-object added
   */
  public void addAction(Action action) {
    actions.add(action);
  }

  /**
   * Adds a LinkCheck object to list "linkChecks".
   *
   * @param linkCheck is the linkCheck added
   */
  public void addLinkCheck(LinkCheck linkCheck) {
    linkChecks.add(linkCheck);
  }

  /**
   * Checks first if any of the goals to the LinkCheck-objects in
   * the list "linkChecks" are triggered. If one of the linkCheck triggers
   * then set the Link's reference to that LinkCheck's alternate reference and
   * execute the linkCheck's actions on the player. If none of the linkCheck's
   * are triggered then execute the Action-objects in the list "actions" on the
   * player.
   *
   * @param player Is the player to execute the actions upon
   */
  public void executeLink(Player player) {
    boolean linkCheckTriggered = false;
    for (LinkCheck linkCheck : linkChecks) {
      if (linkCheck.checkLinkGoal(player)) {
        linkCheckTriggered = true;
        linkCheck.executeActions(player);
        this.reference = linkCheck.getAlternateReference();
        break;
      }
    }

    if (!linkCheckTriggered) {
      for (Action action : actions) {
        action.execute(player);
      }
    }
  }

  public void setToOriginalReference() {
    setReference(originalReference);
  }

  /**
   * Get-method for the list "actions".
   *
   * @return actions
   */
  public List<Action> getActions() {
    return actions;
  }


  /**
   * Returns a textual representation of the Link. Format:
   * Text -> Reference
   * Actions: ##
   *
   * @return A string representing the Link
   */
  @Override
  public String toString() {
    return (text + " -> " + reference);
  }

  /**
   * Tests if an object contains equal values to another object.
   * This hash notably does not include the actions and text.
   *
   * @param o The second object being tested
   * @return A boolean value denoting if the two objects are the same
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (this.getClass() != o.getClass()) {
      return false;
    }

    Link link = (Link) o;
    return reference.equals(link.getReference());
  }

  public List<LinkCheck> getLinkChecks() {
    return linkChecks;
  }

  /**
   * Returns a unique hash representing a particular instance of a Link.
   * This hash notably does not include the actions and text.
   *
   * @return An integer value that acts as a hash for the object
   */
  @Override
  public int hashCode() {
    return reference.hashCode();
  }
}
