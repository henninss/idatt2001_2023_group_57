package no.ntnu.idatt2001.actions;

import no.ntnu.idatt2001.Player;

/**
 * This class represents an action that adds a specified a Player's amount of health.
 * It implements the Action interface
 */
public class HealthAction implements Action {
  private final int health;

  /**
   * Constructs a new HealthAction with the specified amount of health.
   *
   * @param health Is the amount of health the players amount is changed by.
   */
  public HealthAction(int health) {
    this.health = health;
  }

  /**
   * Executes the HealthAction on the specified player by adding the specified amount of health.
   *
   * @param player Is the player to execute the action upon
   */
  @Override
  public void execute(Player player) {
    player.addHealth(health);
  }
}