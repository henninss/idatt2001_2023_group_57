package no.ntnu.idatt2001.actions;

import no.ntnu.idatt2001.Player;

/**
 * This interface represents an action that can be executed on a player.
 */
public interface Action {
  /**
   * Executes the action on the specified Player-object.
   *
   * @param player Is the player to execute the action upon
   */
  void execute(Player player);

}

