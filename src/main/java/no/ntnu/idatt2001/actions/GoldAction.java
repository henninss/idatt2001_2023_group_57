package no.ntnu.idatt2001.actions;

import no.ntnu.idatt2001.Player;

/**
 * This class represents an action that adds a specified a Player's amount of gold.
 * It implements the Action interface
 */
public class GoldAction implements Action {

  private final int gold;

  /**
   * Constructs a new GoldAction with the specified amount of gold.
   *
   * @param gold Is the amount of gold the players amount is changed by.
   */
  public GoldAction(int gold) {
    this.gold = gold;
  }

  /**
   * Executes the GoldAction on the specified player by adding the specified amount of gold.
   *
   * @param player Is the player to execute the action upon
   */
  @Override
  public void execute(Player player) {
    player.addGold(gold);
  }
}
