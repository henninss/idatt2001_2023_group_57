package no.ntnu.idatt2001.actions;

import no.ntnu.idatt2001.Player;

/**
 * This class represents an action that adds a specified a Player's amount of score.
 * It implements the Action interface
 */
public class ScoreAction implements Action {
  private final int score;

  /**
   * Constructs a new ScoreAction with the specified amount of score.
   *
   * @param score Is the amount of score the players amount is changed by.
   */
  public ScoreAction(int score) {
    this.score = score;
  }

  /**
   * Executes the ScoreAction on the specified player by adding the specified amount of score.
   *
   * @param player Is the player to execute the action upon
   */
  @Override
  public void execute(Player player) {
    player.addScore(score);
  }
}
