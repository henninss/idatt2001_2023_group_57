package no.ntnu.idatt2001.actions;

import no.ntnu.idatt2001.Player;

/**
 * This class represents an action that adds an item to the player's inventory.
 * It implements the Action interface
 */
public class InventoryAction implements Action {
  private final String item;

  /**
   * Constructs a new ItemAction with a specified item.
   *
   * @param item Is the item that the ItemAction adds to the player's inventory
   */
  public InventoryAction(String item) {
    this.item = item;
  }

  /**
   * Executes the InventoryAction on the specified player by adding the specified items to their
   * inventory. If the string starts with the symbol "-", then that item gets removed from the
   * player inventory instead.
   *
   * @param player Is the player to execute the action upon
   */
  @Override
  public void execute(Player player) {
    if (item.startsWith("-")) {
      String itemToBeRemoved = item.substring(1);
      player.getInventory().remove(itemToBeRemoved);
    } else {
      player.addToInventory(item);
    }
  }
}