package no.ntnu.idatt2001;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.idatt2001.goals.Goal;
import no.ntnu.idatt2001.goals.GoldGoal;
import no.ntnu.idatt2001.goals.HealthGoal;
import no.ntnu.idatt2001.goals.InventoryGoal;
import no.ntnu.idatt2001.goals.ScoreGoal;
import org.json.JSONException;

/**
 * The main class of the game. This class is responsible for running the game, handling all the
 * user input, game logic, and the graphical user interface.
 */
public class MainGui extends Application {
  Game game;
  Story story;
  Player player;
  Player playerStart;
  BorderPane rootPane;
  VBox gameBox;
  Text gameBoxText;
  List<Goal> presetGoals = new ArrayList<>();
  final List<Goal> achievedGoals = new ArrayList<>();
  VBox statsBox;
  Passage currentPassage;
  Button undo;

  /**
   * Starts the program.
   *
   * @param args Command line arguments
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Updates the box with the player statistics.
   */
  private void updateStatsBox() {
    statsBox.getChildren().clear();
    statsBox.getChildren().add(new Text("Name: " + player.getName()));
    statsBox.getChildren().add(new Text("Health: " + player.getHealth()));
    statsBox.getChildren().add(new Text("Gold: " + player.getGold()));
    statsBox.getChildren().add(new Text("Score: " + player.getScore()));
    statsBox.getChildren().add(new Text("Inventory: "));
    ScrollPane scrollPane = new ScrollPane();
    VBox inventoryList = new VBox();
    scrollPane.setContent(inventoryList);
    inventoryList.setPadding(new Insets(10));
    inventoryList.setSpacing(15);

    player.getInventory().forEach((n) -> {
      Text item = new Text(n);
      item.setWrappingWidth(100);
      item.setTextAlignment(TextAlignment.CENTER);
      inventoryList.getChildren().add(item);
    });


    statsBox.getChildren().add(scrollPane);
  }

  /**
   * First method to be called when the program starts. Sets up the graphical user interface.
   *
   * @param primaryStage The primary stage for this application, onto which the application scene
   *                     can be set.
   */
  @Override
  public void start(Stage primaryStage) {
    rootPane = new BorderPane();
    gameBox = new VBox();
    gameBox.setMaxSize(600, 450);
    // Set a border around the gamePane
    gameBox.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID,
        CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    gameBox.setAlignment(Pos.CENTER);
    // Add the gamePane to the rootPane
    rootPane.setCenter(gameBox);

    // Sample text in the gamePane
    gameBoxText = new Text("Start out by loading a story file.");
    gameBoxText.setTextAlignment(TextAlignment.CENTER);
    gameBoxText.setWrappingWidth(500);
    gameBoxText.setFont(new Font(20));
    gameBox.getChildren().add(gameBoxText);

    // Buttons to the side of the gamePane
    VBox buttonBox = new VBox(10);
    buttonBox.setPadding(new Insets(10));
    buttonBox.setAlignment(Pos.CENTER);
    // Add the buttonBox to the rootPane
    rootPane.setRight(buttonBox);

    // Stats to the side
    statsBox = new VBox(10);
    statsBox.setPadding(new Insets(10));
    statsBox.setAlignment(Pos.CENTER);
    // Add the statsPane to the rootPane
    rootPane.setLeft(statsBox);


    // Create buttons
    Button startGame = new Button("Start Game");
    startGame.setDisable(true);
    Button loadGame = new Button("Load Story");
    loadGame.setDisable(false);
    Button save = new Button("Save");
    save.setDisable(true);
    Button loadSave = new Button("Load Save");
    loadSave.setDisable(true);
    Button restart = new Button("Restart");
    restart.setDisable(true);
    Button newCharacter = new Button("New Character");
    newCharacter.setDisable(true);
    Button addGoals = new Button("Add goals");
    addGoals.setDisable(true);
    undo = new Button("Undo");
    undo.setDisable(true);
    Button reset = new Button("Hard reset");

    // Add the buttons to the buttonBox
    buttonBox.getChildren().addAll(startGame, loadGame, save, loadSave, restart, newCharacter,
        addGoals, undo, reset);

    // Make event listeners for buttons

    // Logic for adding a new character. Opens a new window.
    newCharacter.setOnAction(event -> {
      BorderPane pane = makeNewCharacterPane();

      Stage stage = new Stage();
      stage.setTitle("New character");
      stage.setScene(new Scene(pane));
      stage.showAndWait();
      if (player != null) {
        loadSave.setDisable(true);
        startGame.setDisable(false);
        newCharacter.setText("Edit character");
      }
    });

    // Logic for adding goals. Opens a new window.
    addGoals.setOnAction(actionEvent -> {
      BorderPane pane = makeAddGoalsPane();

      Stage stage = new Stage();
      stage.setTitle("Add goals");
      stage.setScene(new Scene(pane));
      stage.showAndWait();
    });

    // Logic for loading a game. Opens a file chooser.
    loadGame.setOnAction(new EventHandler<>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose a .paths file");
        fileChooser.getExtensionFilters().add(
            new FileChooser.ExtensionFilter("Paths files", "*.paths"));
        fileChooser.setInitialDirectory(
            new File(Objects.requireNonNull(
                getClass().getClassLoader().getResource("stories")).getPath()));
        try {
          gameBox.getChildren().clear();
          gameBox.getChildren().add(gameBoxText);
          story = PathsFileController.readStory(
              fileChooser.showOpenDialog(rootPane.getScene().getWindow()).getAbsolutePath());
          List<Link> brokenLinks = story.getBrokenLinks();

          if (!brokenLinks.isEmpty()) {
            Text brokenLinksText = new Text("The game contains " + brokenLinks.size()
                + " broken links. This means some passages in the game are unreachable. "
                + "The broken links are: " + brokenLinks);
            brokenLinksText.setWrappingWidth(500);
            brokenLinksText.setTextAlignment(TextAlignment.CENTER);
            brokenLinksText.setFont(new Font(12));
            gameBox.getChildren().add(brokenLinksText);
          }

          newCharacter.setDisable(false);
          addGoals.setDisable(false);
          loadSave.setDisable(false);
          loadGame.setDisable(true);
          gameBoxText.setText("Story loaded. Create a new character and add optional goals to "
              + "start. Alternatively, load a save file. "
              + "Press the start game button to start playing.");
        } catch (IOException e) {
          gameBoxText.setText("Something went wrong with loading the story. "
              + "Try again, or check the console for more details.");
          e.printStackTrace();
        }
      }
    });

    // Logic for loading a save. Opens a file chooser.
    loadSave.setOnAction(new EventHandler<>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose a .save file");
        fileChooser.getExtensionFilters().add(
            new FileChooser.ExtensionFilter("Save files", "*.save"));
        fileChooser.setInitialDirectory(
            new File(Objects.requireNonNull(
                getClass().getClassLoader().getResource("saves")).getPath()));
        try {
          game = SaveFileController.readSave(fileChooser.showOpenDialog(
              rootPane.getScene().getWindow()).getAbsolutePath(), story);
          player = game.getPlayer();
          playerStart = player.getPlayerCopy();
          presetGoals = game.getGoals();
          startGame.setDisable(false);
          newCharacter.setDisable(true);
          addGoals.setDisable(true);
        } catch (IOException | JSONException e) {
          gameBoxText.setText("Something went wrong with loading the save. "
              + "Try again, or check the console for more details.");
          e.printStackTrace();
        }
      }
    });

    // Logic for saving a game once it has started. Opens a file chooser.
    save.setOnAction(new EventHandler<>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose a .save file");
        fileChooser.getExtensionFilters().add(
            new FileChooser.ExtensionFilter("Save files", "*.save"));
        fileChooser.setInitialDirectory(
            new File(Objects.requireNonNull(
                getClass().getClassLoader().getResource("saves")).getPath()));
        try {
          SaveFileController.writeSave(
              fileChooser.showSaveDialog(rootPane.getScene().getWindow()).getAbsolutePath(), game);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });


    // Logic for starting a game once a character has been created and goals have been added. Or
    // if a save has been loaded.
    startGame.setOnAction(actionEvent -> {
      game = new Game(player, story, presetGoals);
      save.setDisable(false);
      newCharacter.setDisable(true);
      restart.setDisable(false);
      startGame.setDisable(true);
      addGoals.setDisable(true);
      startPlaying();
    });

    // Logic for undoing a move between passages.
    undo.setOnAction(actionEvent -> {
      if (game != null) {
        gameBox = generatePassageBox(game.undo());
        updateStatsBox();
        rootPane.setCenter(gameBox);
      }
    });

    // Logic for restarting the game.
    restart.setOnAction(actionEvent -> {
      if (game != null) {
        player = playerStart.getPlayerCopy();
        game = new Game(player, story, presetGoals);
        gameBox = generatePassageBox(game.begin());
        updateStatsBox();
        rootPane.setCenter(gameBox);
      }
    });

    // Logic for resetting the entire program. Opens a new instance of the program, and closes the
    // current one.
    reset.setOnAction(actionEvent -> {
      try {
        primaryStage.hide();
        start(new Stage());
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    });

    Scene scene = new Scene(rootPane, 900, 600);
    primaryStage.setTitle("Game");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  /**
   * When you first start playing, this method is called to generate the passage box for the first
   * passage.
   */
  private void startPlaying() {
    updateStatsBox();
    gameBox = generatePassageBox(game.begin());
    rootPane.setCenter(gameBox);
  }

  /**
   * Generates a new window where the user can input the name, health and gold of their character.
   *
   * @return the pane containing the new character window.
   */
  private BorderPane makeNewCharacterPane() {
    VBox characterBox = new VBox();
    characterBox.setPadding(new Insets(10));
    characterBox.setSpacing(10);
    characterBox.setAlignment(Pos.CENTER);

    TextField nameField = new TextField();
    nameField.setPromptText("Name");
    TextField healthField = new TextField();
    healthField.setPromptText("Health");
    TextField goldField = new TextField();
    goldField.setPromptText("Gold");

    Button createCharacter = new Button("Create character");
    Text characterStatus = new Text("");

    // Logic for creating a new character once data has been inputted. Uses a builder to create a
    // new player.
    createCharacter.setOnAction(actionEvent -> {
      if (nameField.getText().isBlank()) {
        characterStatus.setText("Name field must be filled out");
        return;
      }

      try {
        if (Integer.parseInt(healthField.getText()) <= 0
            || Integer.parseInt(goldField.getText()) < 0) {
          throw new NumberFormatException();
        }

        player = new Player.Builder(nameField.getText())
            .health(Integer.parseInt(healthField.getText()))
            .gold(Integer.parseInt(goldField.getText()))
            .build();

        characterStatus.setText("Character created");
      } catch (NumberFormatException e) {
        // If we catch, use the default builder
        player = new Player.Builder(nameField.getText()).build();
        characterStatus.setText("Character created with default values (Health: 100, Gold: 0)");
      }

      // We make two copies of the player, one to be used as the current player,
      // and one to be used as the starting player
      playerStart = player.getPlayerCopy();
      updateStatsBox();
    });


    Text characterInfo = new Text("Create a new character, or edit your existing one. "
        + "Name must be filled out. Health and gold must be integers and above 0. "
        + "\nThey will be set to default values if one or more field is not filled "
        + "out or if wrongly filled in. (recommended)");

    characterBox.getChildren().addAll(characterInfo, nameField, healthField, goldField,
        createCharacter, characterStatus);

    BorderPane pane = new BorderPane();
    pane.setCenter(characterBox);

    return pane;
  }

  /**
   * Generates a new window where the user can input goals for the game.
   *
   * @return the pane containing the add goals window.
   */
  private BorderPane makeAddGoalsPane() {
    VBox goalsBox = new VBox();
    goalsBox.setPadding(new Insets(10));
    goalsBox.setSpacing(10);
    goalsBox.setAlignment(Pos.CENTER);

    Text goalInfo = new Text("Add goals to the game. When a goal is reached, "
        + "your inputted score value will be added to the total score. The score given can not be"
        + "negative, neither can the value that must be reached. Multiple goals can be added. "
        + "You can press the reset button to remove all goals. All numbers must be integers. "
        + "Adding items is done by writing items, then pressing the add item"
        + " button, then finally the add goal button.");
    goalInfo.setWrappingWidth(300);
    goalInfo.setTextAlignment(TextAlignment.CENTER);

    TextField goalValue = new TextField();
    goalValue.setPromptText("Goal value/Item name");

    TextField scoreValue = new TextField();
    scoreValue.setPromptText("Score given");

    Text goalStatus = new Text("Status: ");

    Button addItem = new Button("Add item");
    addItem.setVisible(false);

    ArrayList<String> items = new ArrayList<>();

    String[] goalTypes = {"Score", "Health", "Inventory", "Gold"};
    ComboBox<String> goalSelect = new ComboBox<>(FXCollections.observableArrayList(goalTypes));

    // Logic for when the combo box is changed. If the user selects inventory, the add item button
    // will be made visible. If not, it will be hidden.
    goalSelect.addEventHandler(ActionEvent.ACTION, actionEvent -> {
      if (goalSelect.getValue().equals("Inventory")) {
        addItem.setVisible(true);
      } else {
        addItem.setVisible(false);
        items.clear();
        goalStatus.setText("Status: " + "Item list cleared.");
      }
    });

    // Logic for adding an item to the tentative item list.
    addItem.setOnAction(actionEvent -> {
      if (goalValue.getText().isBlank()) {
        goalStatus.setText("Status: " + "You did not input anything! Item was not added.");
        return;
      }

      if (items.contains(goalValue.getText())) {
        goalStatus.setText("Status: " + "You already have this item in the Goal! "
            + "Item was not added.");
        return;
      }

      items.add(goalValue.getText());
      goalStatus.setText("Status: " + "Item " + goalValue.getText() + " was added.");
      goalValue.clear();
    });

    Button addGoal = new Button("Add goal");
    // Logic for adding a goal to the game. Adds the finished goal to the preset goals list.
    addGoal.setOnAction(actionEvent -> {
      try {
        if (Integer.parseInt(goalValue.getText()) < 0
            || Integer.parseInt(scoreValue.getText()) < 0) {
          goalStatus.setText("Status: " + "Your input is negative! Goal was not added.");
          return;
        }
      } catch (NumberFormatException ignored) {
        // Ignored
      }


      try {
        switch (goalSelect.getValue()) {
          case "Score" ->
              presetGoals.add(new ScoreGoal(Integer.parseInt(goalValue.getText()),
                  Integer.parseInt(scoreValue.getText())));
          case "Health" ->
              presetGoals.add(new HealthGoal(Integer.parseInt(goalValue.getText()),
                  Integer.parseInt(scoreValue.getText())));
          case "Inventory" -> {
            if (items.isEmpty()) {
              goalStatus.setText("Status: "
                  + "You have not added any items! Goal was not added.");
              return;
            }

            // Make a copy so that clearing the items list does not affect the goal
            ArrayList<String> itemsCopy = new ArrayList<>(items);
            presetGoals.add(new InventoryGoal(itemsCopy, Integer.parseInt(scoreValue.getText())));
            items.clear();
          }
          case "Gold" ->
              presetGoals.add(new GoldGoal(Integer.parseInt(goalValue.getText()),
                  Integer.parseInt(scoreValue.getText())));
          default ->
              throw new IllegalStateException("Unexpected value: " + goalSelect.getValue());
        }


        goalStatus.setText("Status: " + goalSelect.getValue() + " goal added.");
      } catch (NumberFormatException e) {
        goalStatus.setText("Status: " + "Your input is not a number! Goal was not added.");
      } catch (NullPointerException e) {
        goalStatus.setText("Status: " + "You have not selected a goal type! Goal was not added.");
      }

      goalValue.clear();
      scoreValue.clear();
    });

    Button resetGoals = new Button("Reset goals");
    // Logic for resetting the preset goals list.
    resetGoals.setOnAction(actionEvent -> {
      presetGoals.clear();
      goalStatus.setText("Status: " + "Goals reset.");
    });

    goalsBox.getChildren().addAll(goalInfo, goalSelect, goalValue, addItem, scoreValue,
        goalStatus, addGoal, resetGoals);

    BorderPane pane = new BorderPane();
    pane.setCenter(goalsBox);

    return pane;
  }

  /**
   * Generates a graphical interface of a passage. This method is called recursively, to generate
   * all the passages in the game.
   *
   * @param passage the passage to generate the interface for
   * @return a VBox containing the passage's title, content, and links
   */
  private VBox generatePassageBox(Passage passage) {
    currentPassage = passage;

    VBox passageBox = new VBox();

    passageBox.setMaxSize(600, 450);
    passageBox.setSpacing(25);
    // Set a border around the gamePane
    passageBox.setBorder(new Border(new BorderStroke(Color.BLACK,
        BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    passageBox.setAlignment(Pos.CENTER);

    Text passageTitle = new Text(passage.getTitle());
    passageTitle.setFont(new Font(20));
    passageBox.getChildren().add(passageTitle);

    Text passageContent = new Text(passage.getContent());
    passageContent.setWrappingWidth(500);
    passageContent.setTextAlignment(TextAlignment.CENTER);
    passageContent.setFont(new Font(12));
    passageBox.getChildren().add(passageContent);

    for (Link l : passage.getLinks()) {
      Button linkButton = new Button(l.getText());
      // Logic for when the link button is clicked. Updates the gamePane to the next passage.
      linkButton.setOnAction(actionEvent -> {
        currentPassage = game.go(l);

        for (Goal g : game.getGoals()) {
          if (g.isFulfilled(player) && !achievedGoals.contains(g)) {
            player.addScore(g.getScore());
            achievedGoals.add(g);
          }
        }

        if (currentPassage.getTitle().equals("End of Story")) {
          gameBox = generateEndBox();
        } else if (player.getHealth() > 0) {
          undo.setDisable(!game.hasOtherGameStates());
          gameBox = generatePassageBox(currentPassage);
        } else {
          gameBox = generateDeathBox();
        }

        rootPane.setCenter(gameBox); // !!
        updateStatsBox();
      });
      passageBox.getChildren().add(linkButton);
    }

    return passageBox;
  }

  /**
   * Generates a graphical interface for when the player dies.
   *
   * @return a VBox containing the death message
   */
  private VBox generateDeathBox() {
    VBox deathBox = new VBox();

    deathBox.setMaxSize(600, 450);
    deathBox.setSpacing(25);
    // Set a border around the gamePane
    deathBox.setBorder(new Border(new BorderStroke(Color.BLACK,
        BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    deathBox.setAlignment(Pos.CENTER);

    Text title = new Text("Game Over!");
    title.setFont(new Font(20));
    deathBox.getChildren().add(title);

    Text content = new Text("You perished from your wounds...");
    content.setFont(new Font(12));
    deathBox.getChildren().add(content);

    return deathBox;
  }

  /**
   * Generates a graphical interface for when the player completes the game.
   *
   * @return a VBox containing the completion message
   */
  private VBox generateEndBox() {
    VBox endBox = new VBox();

    endBox.setMaxSize(600, 450);
    endBox.setSpacing(25);
    // Set a border around the gamePane
    endBox.setBorder(new Border(new BorderStroke(Color.BLACK,
        BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    endBox.setAlignment(Pos.CENTER);

    Text title = new Text("Congratulations! You have completed the game!");
    title.setFont(new Font(20));
    endBox.getChildren().add(title);

    Text score = new Text("Final Score: " + player.getScore());
    score.setFont(new Font(12));
    endBox.getChildren().add(score);

    StringBuilder scoreSummary = new StringBuilder("Score Summary: \n");
    for (Goal g : achievedGoals) {
      scoreSummary.append("- ").append(g.getScore()).append(" points for getting ")
          .append(g).append("\n");

    }

    Text scoreSummaryText = new Text(scoreSummary.toString());
    scoreSummaryText.setFont(new Font(12));
    endBox.getChildren().add(scoreSummaryText);

    return endBox;
  }
}