package no.ntnu.idatt2001;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import no.ntnu.idatt2001.actions.Action;
import no.ntnu.idatt2001.actions.GoldAction;
import no.ntnu.idatt2001.actions.HealthAction;
import no.ntnu.idatt2001.actions.InventoryAction;
import no.ntnu.idatt2001.actions.ScoreAction;
import no.ntnu.idatt2001.goals.Goal;
import no.ntnu.idatt2001.goals.GoldGoal;
import no.ntnu.idatt2001.goals.HealthGoal;
import no.ntnu.idatt2001.goals.InventoryGoal;
import no.ntnu.idatt2001.goals.ScoreGoal;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is responsible for reading a story from a file and returning a Story object.
 * It accomplishes this according to the requirements provided, and with JSON as a supplemental
 * data format. Certain methods have vulnerability warnings disabled, as these vulnerabilities
 * are not relevant to the project, and a JSON library artifact.
 */
public class PathsFileController {

  /**
   * Reads the main .paths file and returns a Story object
   *
   * @param path Path to the .paths file
   * @return Story object with all passages and links added
   * @throws IOException If the file is badly formatted or another error occurs when reading
   */
  public static Story readStory(String path) throws IOException {
    File file = new File(path);
    BufferedReader br = new BufferedReader(new FileReader(file));

    String st;
    String storyTitle = "";
    ArrayList<Passage> passages = new ArrayList<>();
    boolean titleSet = false;

    // Read each line one-by-one until EOF, or if we want to skip a line.
    // br.readline() does not skip empty lines
    while ((st = br.readLine()) != null) {
      // The first line (which contains the title)
      if (!titleSet) {
        storyTitle = st;
        if (storyTitle.isEmpty()) {
          throw new IOException("File is badly formatted (story title is empty)");
        }

        st = br.readLine();
        titleSet = true;
      }

      // If line is empty, skip it
      if (!st.isEmpty()) {
        // Start the whole process of reading a Passage
        if (st.startsWith("::")) {
          passages.add(readPassage(br, st));
        } else {
          throw new IOException("File is badly formatted (could not find start of passage '::')");
        }
      }
    }

    // After title and all passages are read, construct the story and return it
    Story story = new Story(storyTitle, passages.remove(0));
    for (Passage p : passages) {
      story.addPassage(p);
    }

    return story;
  }

  /**
   * Reads a passage from the .paths file and returns a Passage object
   *
   * @param br The BufferedReader object that is reading the file
   * @param st The current line that is being read
   * @return Passage object with all links added
   * @throws IOException If the file is badly formatted or another error occurs when reading
   */
  private static Passage readPassage(BufferedReader br, String st) throws IOException {
    // Remove the '::' from the title
    String title = st.replace(":", "");
    String content = br.readLine();
    ArrayList<Link> links = new ArrayList<>();

    st = br.readLine();
    // Read all links until we get an empty line or EOF.
    // Querying st will return null if EOF, so we try-catch it
    try {
      while (!st.isEmpty()) {
        Link currentLink;

        // Regex that matches the pattern [link text](link destination) json
        // The JSON is optional, and can be empty. It contains actions and link checks
        Pattern p = Pattern.compile("\\[(.+?)]\\((.+?)\\)(.*)");
        Matcher m = p.matcher(st);

        if (m.find()) {
          currentLink = new Link(m.group(1), m.group(2));
        } else {
          throw new IOException("Regex did not find any matching pattern "
              + "(Link is incorrectly formatted)");
        }

        // Read in all the actions and link checks, if they exist
        if (!m.group(3).isBlank()) {
          try {
            readJson(currentLink, m.group(3));
          } catch (JSONException e) {
            throw new IOException("Characters detected after link reference, "
                + "but could not parse as JSON. (Link is incorrectly formatted)");
          }
        }


        links.add(currentLink);

        st = br.readLine();
      }
    } catch (NullPointerException ignored) {
      // EOF
    }

    // Finally construct the passage
    Passage newPassage = new Passage(title, content);
    for (Link l : links) {
      newPassage.addLink(l);
    }

    return newPassage;
  }

  /**
   * Reads all actions and link checks from a JSON format and adds them to the Link object.
   *
   * @param link Link object to add the actions and link checks to
   * @param json The string containing the JSON
   */
  @SuppressWarnings("VulnerableCodeUsages")
  private static void readJson(Link link, String json) {
    // Make a JSON object from the JSON string
    JSONObject jsonObject = new JSONObject(json);
    ArrayList<Action> actions;
    ArrayList<LinkCheck> linkChecks;

    if (jsonObject.has("actions")) {
      actions = readActions(jsonObject.getJSONArray("actions"));
      for (Action a : actions) {
        link.addAction(a);
      }
    }

    if (jsonObject.has("linkChecks")) {
      linkChecks = readLinkChecks(jsonObject.getJSONArray("linkChecks"));
      for (LinkCheck lc : linkChecks) {
        link.addLinkCheck(lc);
      }
    }
  }

  /**
   * Reads all link checks from the relevant JSON array, and returns them in an ArrayList.
   *
   * @param linkChecksJson The JSON array containing the link checks
   * @return ArrayList of LinkCheck objects
   */
  private static ArrayList<LinkCheck> readLinkChecks(JSONArray linkChecksJson) {
    ArrayList<LinkCheck> linkChecks = new ArrayList<>();

    for (int i = 0; i < linkChecksJson.length(); i++) {
      JSONObject jsonLinkCheck = linkChecksJson.getJSONObject(i);
      LinkCheck linkCheck = new LinkCheck(jsonLinkCheck.getString("reference"),
          readGoal(jsonLinkCheck.getJSONArray("goal")));

      if (jsonLinkCheck.has("actions")) {
        for (Action a : readActions(jsonLinkCheck.getJSONArray("actions"))) {
          linkCheck.addAction(a);
        }
      }

      linkChecks.add(linkCheck);
    }

    return linkChecks;
  }

  /**
   * Reads a goal from the relevant JSON array, and returns it.
   *
   * @param goalJson The JSON array containing the goal
   * @return Goal read from the JSON array
   */
  @SuppressWarnings("VulnerableCodeUsages")
  private static Goal readGoal(JSONArray goalJson) {
    String goalType = goalJson.getString(0);
    Goal goal;

    switch (goalType) {
      case "H" -> goal = new HealthGoal(goalJson.getInt(1));
      case "G" -> goal = new GoldGoal(goalJson.getInt(1));
      case "S" -> goal = new ScoreGoal(goalJson.getInt(1));
      case "I" -> {
        ArrayList<String> items = new ArrayList<>();
        for (int i = 1; i < goalJson.length(); i++) {
          items.add(goalJson.getString(i));
        }
        goal = new InventoryGoal(items);
      }
      default ->
          throw new IllegalStateException("Unexpected value: " + goalType);
    }

    return goal;
  }

  /**
   * Reads all actions from the relevant JSON array, and returns them in an ArrayList.
   *
   * @param jsonActions The JSON array containing the actions
   * @return ArrayList of Action objects
   */
  @SuppressWarnings("VulnerableCodeUsages")
  private static ArrayList<Action> readActions(JSONArray jsonActions) {
    ArrayList<Action> actions = new ArrayList<>();
    for (Object action : jsonActions) {
      JSONArray jsonAction = (JSONArray) action;
      String actionType = jsonAction.getString(0);

      switch (actionType) {
        case "H" -> actions.add(new HealthAction(jsonAction.getInt(1)));
        case "G" -> actions.add(new GoldAction(jsonAction.getInt(1)));
        case "S" -> actions.add(new ScoreAction(jsonAction.getInt(1)));
        case "I" -> actions.add(new InventoryAction(jsonAction.getString(1)));
        default -> throw new IllegalStateException("Unexpected value: " + actionType);
      }
    }

    return actions;
  }
}
