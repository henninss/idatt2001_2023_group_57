package no.ntnu.idatt2001;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import no.ntnu.idatt2001.goals.Goal;
import no.ntnu.idatt2001.goals.GoldGoal;
import no.ntnu.idatt2001.goals.HealthGoal;
import no.ntnu.idatt2001.goals.InventoryGoal;
import no.ntnu.idatt2001.goals.ScoreGoal;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is responsible for reading a saved game from a .save file. The whole save file is
 * formatted with JSON. The save file required a story object to be passed in, as the save file
 * is unique to a story. Certain methods have vulnerability warnings disabled, as these
 * vulnerabilities are not relevant to the project, and a JSON library artifact.
 */
public class SaveFileController {

  /**
   * Reads a save file and returns a Game object.
   *
   * @param path Path to the save file
   * @param story Story object that the save file is for
   * @return Game object with all the data from the save file
   * @throws IOException If the file is badly formatted or another error occurs when reading
   * @throws JSONException If the file is badly formatted
   */
  @SuppressWarnings("VulnerableCodeUsages")
  public static Game readSave(String path, Story story) throws IOException, JSONException {
    // Player is saved as a json file with the following format:
    // Goals (Array of goals where each goal is an array with [goalType, goalValue])
    // Name
    // Health
    // Score
    // Gold
    // Inventory
    // Current passage (Title of the passage)
    // Current story (Title of the story)

    File file = new File(path);
    String content = FileUtils.readFileToString(file, "utf-8");
    JSONObject json = new JSONObject(content);

    ArrayList<Goal> goals = new ArrayList<>();

    // Read goals
    // Every goal except InventoryGoal has the same format:
    // [goalType, goalValue]
    // InventoryGoal has the format:
    // [goalType, item1, item2, ...]

    JSONArray goalsJson = json.getJSONArray("goals");
    for (Object goal : goalsJson) {
      JSONArray goalJson = (JSONArray) goal;
      String goalType = goalJson.getString(0);

      switch (goalType) {
        case "H" -> goals.add(new HealthGoal(goalJson.getInt(1)));
        case "G" -> goals.add(new GoldGoal(goalJson.getInt(1)));
        case "S" -> goals.add(new ScoreGoal(goalJson.getInt(1)));
        case "I" -> {
          ArrayList<String> items = new ArrayList<>();
          // Start the index at 1 to skip the goalType
          for (int i = 1; i < goalJson.length(); i++) {
            items.add(goalJson.getString(i));
          }
          goals.add(new InventoryGoal(items));
        }
        default ->
            throw new IllegalStateException("Unexpected value: " + goalType);
      }
    }


    ArrayList<String> inventory = new ArrayList<>();
    JSONArray inventoryJson = json.getJSONArray("inventory");
    for (Object item : inventoryJson) {
      inventory.add((String) item);
    }

    if (!Objects.equals(json.getString("storyTitle"), story.getTitle())) {
      throw new IllegalArgumentException("Story hash does not match "
          + "(Save file is for a different story)");
    }

    Passage currentPassage = null;

    for (Passage passage : story.getPassages()) {
      if (Objects.equals(passage.getTitle(), json.getString("passageTitle"))) {
        currentPassage = passage;
      }
    }

    // story.getPassages does not include the opening passage
    if (Objects.equals(story.getOpeningPassage().getTitle(), json.getString("passageTitle"))) {
      currentPassage = story.getOpeningPassage();
    }

    if (currentPassage == null) {
      throw new IllegalArgumentException("Passage title does not match "
          + "(Could not find passage player is in story)");
    }

    Player player = new Player.Builder(json.getString("name"))
        .health(json.getInt("health"))
        .gold(json.getInt("gold"))
        .score(json.getInt("score"))
        .inventory(inventory)
        .build();

    story.setOpeningPassage(currentPassage);

    return new Game(player, story, goals);
  }

  /**
   * Writes a save file for a game.
   *
   * @param path Path to the save file
   * @param game Game object to save
   * @throws IOException If an error occurs when writing
   */
  @SuppressWarnings("VulnerableCodeUsages")
  public static void writeSave(String path, Game game) throws IOException {
    // Player is saved as a json file with the following format:
    // Goals (Array of goals where each goal is an array with [goalType, goalValue, goalScore])
    // Name
    // Health
    // Score
    // Gold
    // Inventory
    // Current passage (Title of the passage)
    // Current story (Title of the story)

    JSONObject json = new JSONObject();

    // Write goals
    // Every goal except InventoryGoal has the same format
    // [goalType, goalValue, goalScore]
    // InventoryGoal has the format
    // [goalType, goalValue, item1, item2, ...]

    JSONArray goalsJson = new JSONArray();
    for (Goal goal : game.getGoals()) {
      JSONArray goalJson = new JSONArray();
      if (goal instanceof HealthGoal) {
        goalJson.put("H");
        goalJson.put(((HealthGoal) goal).getMinimumHealth(), goal.getScore());
      } else if (goal instanceof GoldGoal) {
        goalJson.put("G");
        goalJson.put(((GoldGoal) goal).getMinimumGold(), goal.getScore());
      } else if (goal instanceof ScoreGoal) {
        goalJson.put("S");
        goalJson.put(((ScoreGoal) goal).getMinimumPoints(), goal.getScore());
      } else if (goal instanceof InventoryGoal) {
        goalJson.put("I");
        goalJson.put(goal.getScore());
      }


      if (goal instanceof InventoryGoal) {
        for (String item : ((InventoryGoal) goal).getMandatoryItems()) {
          goalJson.put(item);
        }
      }

      goalsJson.put(goalJson);
    }

    json.put("goals", goalsJson);
    json.put("name", game.getPlayer().getName());
    json.put("health", game.getPlayer().getHealth());
    json.put("score", game.getPlayer().getScore());
    json.put("gold", game.getPlayer().getGold());

    JSONArray inventoryJson = new JSONArray();
    for (String item : game.getPlayer().getInventory()) {
      inventoryJson.put(item);
    }

    json.put("inventory", inventoryJson);
    if (game.getLastGameState().link() == null) {
      json.put("passageTitle", game.getStory().getOpeningPassage().getTitle());
    } else {
      // Gets the last link in the game state and gets the title of the passage it links to
      json.put("passageTitle", game.go(game.getLastGameState().link()).getTitle());
    }
    json.put("storyTitle", game.getStory().getTitle());

    FileUtils.writeStringToFile(new File(path), json.toString(), "utf-8");
  }
}
