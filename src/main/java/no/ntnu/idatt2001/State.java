package no.ntnu.idatt2001;

/**
 * This class represents a state of the game, with a player and a link. It is used to rewind
 * the game's state. (Undo)
 */
public record State(Player player, Link link) {
  /**
   * Constructor for a State object.
   *
   * @param player Is the Player-object stored in the State.
   * @param link   Is the Link-object stored in the State.
   */
  public State {
  }

  /**
   * Get-method for the Player.
   *
   * @return the Player.
   */
  @Override
  public Player player() {
    return player;
  }

  /**
   * Get method for the Link.
   *
   * @return the Link.
   */
  @Override
  public Link link() {
    return link;
  }
}
