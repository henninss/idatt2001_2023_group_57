package no.ntnu.idatt2001.goals;

import no.ntnu.idatt2001.Player;

/**
 * This class represents a goal that requires a player to accumulate a minimum amount of score.
 * It implements the Goal interface
 */
public class ScoreGoal implements Goal {
  private final int minimumPoints;
  private int score = 0;

  /**
   * Constructs a new ScoreGoal object with the specified minimum
   * amount of points required to fulfill it.
   *
   * @param minimumPoints Is the minimum score required to fulfill the goal
   * @param score         Is the score the player gets when the goal is fulfilled
   */
  public ScoreGoal(int minimumPoints, int score) {
    this.minimumPoints = minimumPoints;
    this.score = score;
  }

  public ScoreGoal(int minimumPoints) {
    this.minimumPoints = minimumPoints;
  }

  /**
   * Checks whether the goal has been fulfilled by the specified player by comparing the player's
   * score with the minimum required amount.
   *
   * @param player Is the player that gets checked for goal fulfillment
   * @return true if the goal has been fulfilled, otherwise false
   */
  @Override
  public boolean isFulfilled(Player player) {
    return minimumPoints <= player.getScore();
  }

  /**
   * Gets the score of the goal.
   *
   * @return The score of the goal
   */
  @Override
  public int getScore() {
    return score;
  }

  /**
   * Gets the minimum score required to fulfill the goal.
   *
   * @return The minimum score required to fulfill the goal
   */
  public int getMinimumPoints() {
    return minimumPoints;
  }

  /**
   * Returns a string representation of the goal, with the goal's minimum required health.
   *
   * @return A string representation of the goal
   */
  @Override
  public String toString() {
    return minimumPoints + " points";
  }
}