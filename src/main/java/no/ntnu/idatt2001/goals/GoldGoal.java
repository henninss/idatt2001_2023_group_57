package no.ntnu.idatt2001.goals;

import no.ntnu.idatt2001.Player;

/**
 * This class represents a goal that requires a player to accumulate a minimum amount of gold.
 * It implements the Goal interface.
 */
public class GoldGoal implements Goal {
  private final int minimumGold;
  private int score = 0;

  /**
   * Constructs a new GoldGoal object with the specified minimum amount of gold required to
   * fulfill it.
   *
   * @param minimumGold Is the minimum gold required to fulfill the goal
   * @param score       Is the score the player gets when the goal is fulfilled
   */
  public GoldGoal(int minimumGold, int score) {
    this.minimumGold = minimumGold;
    this.score = score;
  }

  public GoldGoal(int minimumGold) {
    this.minimumGold = minimumGold;
  }

  /**
   * Checks whether the goal has been fulfilled by the specified player by comparing the player's
   * gold with the minimum required amount.
   *
   * @param player Is the player that gets checked for goal fulfillment
   * @return true if the goal has been fulfilled, otherwise false
   */
  @Override
  public boolean isFulfilled(Player player) {
    return minimumGold <= player.getGold();
  }

  /**
   * Gets the score of the goal.
   *
   * @return The score of the goal
   */
  @Override
  public int getScore() {
    return score;
  }

  /**
   * Gets the minimum gold required to fulfill the goal.
   *
   * @return The minimum gold required to fulfill the goal
   */
  public int getMinimumGold() {
    return minimumGold;
  }

  /**
   * Returns a string representation of the goal, with the goal's minimum required gold.
   *
   * @return A string representation of the goal
   */
  @Override
  public String toString() {
    return minimumGold + " gold";
  }
}
