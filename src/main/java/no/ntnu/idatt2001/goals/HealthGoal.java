package no.ntnu.idatt2001.goals;

import no.ntnu.idatt2001.Player;

/**
 * This class represents a goal that requires a player to have a minimum amount of health.
 * It implements the Goal interface
 */
public class HealthGoal implements Goal {
  private final int minimumHealth;
  private int score = 0;

  /**
   * Constructs a new HealthGoal object with the specified minimum amount of health required to
   * fulfill it.
   *
   * @param minimumHealth Is the minimum health required to fulfill the goal
   * @param score         Is the score the player gets when the goal is fulfilled
   */
  public HealthGoal(int minimumHealth, int score) {
    this.minimumHealth = minimumHealth;
    this.score = score;
  }

  public HealthGoal(int minimumHealth) {
    this.minimumHealth = minimumHealth;
  }

  /**
   * Checks whether the goal has been fulfilled by the specified player by comparing the player's
   * health with the minimum required amount.
   *
   * @param player Is the player that gets checked for goal fulfillment
   * @return true if the goal has been fulfilled, otherwise false
   */
  @Override
  public boolean isFulfilled(Player player) {
    return minimumHealth <= player.getHealth();
  }

  /**
   * Gets the score of the goal.
   *
   * @return The score of the goal
   */
  @Override
  public int getScore() {
    return score;
  }

  /**
   * Gets the minimum health of the goal.
   *
   * @return The minimum health of the goal
   */
  public int getMinimumHealth() {
    return minimumHealth;
  }

  /**
   * Returns a string representation of the goal, with the goal's minimum required health.
   *
   * @return A string representation of the goal
   */
  @Override
  public String toString() {
    return minimumHealth + " health";
  }
}
