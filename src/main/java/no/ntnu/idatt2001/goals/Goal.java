package no.ntnu.idatt2001.goals;

import no.ntnu.idatt2001.Player;

/**
 * This interface represents a goal that can be fulfilled by a player.
 */
public interface Goal {

  /**
   * Checks whether the goal has been fulfilled by the specified player.
   *
   * @param player Is the player that gets checked for goal fulfillment
   * @return true if the goal has been fulfilled, otherwise false
   */
  boolean isFulfilled(Player player);

  /**
   * Gets the score of the goal.
   *
   * @return The score of the goal
   */
  int getScore();

  /**
   * Returns a string representation of the goal, with the requirements to fulfill it.
   *
   * @return A string representation of the goal
   */
  String toString();
}
