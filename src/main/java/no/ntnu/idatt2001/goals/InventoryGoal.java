package no.ntnu.idatt2001.goals;

import java.util.ArrayList;
import java.util.List;
import no.ntnu.idatt2001.Player;

/**
 * This class represents a goal that requires a player's inventory to include a specified list of
 * items to fulfill the goal. Implements the Goal interface
 */
public class InventoryGoal implements Goal {
  private final List<String> mandatoryItems;
  private int score = 0;

  /**
   * Constructs a new InventoryGoal object with the mandatoryItems to fulfill the goal.
   *
   * @param mandatoryItems Is the mandatory items required to fulfill the goal
   * @param score          Is the score the player gets when the goal is fulfilled
   */
  public InventoryGoal(List<String> mandatoryItems, int score) {
    this.mandatoryItems = mandatoryItems;
    this.score = score;
  }

  /**
   * Constructs a new InventoryGoal object with the mandatoryItems to fulfill the goal, while
   * leaving out the score.
   *
   * @param mandatoryItems Is the mandatory items required to fulfill the goal
   */
  public InventoryGoal(List<String> mandatoryItems) {
    this.mandatoryItems = mandatoryItems;
  }

  /**
   * Checks whether the goal has been fulfilled by the specified player by comparing the player's
   * inventory with the mandatory items specified by the InventoryGoal.
   *
   * @param player Is the player that gets checked for goal fulfillment
   * @return true if the goal has been fulfilled, otherwise false
   */
  @Override
  public boolean isFulfilled(Player player) {
    List<String> playerInventory = player.getInventory();

    if (playerInventory.isEmpty()) {
      return false;
    }

    for (String item : mandatoryItems) {
      if (!playerInventory.contains(item)) {
        return false;
      }
    }
    return true;
  }

  public List<String> getMandatoryItems() {
    return new ArrayList<>(mandatoryItems);
  }

  /**
   * Gets the score of the goal.
   *
   * @return The score of the goal
   */
  @Override
  public int getScore() {
    return score;
  }

  /**
   * Returns a string representation of the goal, with the goal's mandatory items.
   *
   * @return A string representation of the goal
   */
  @Override
  public String toString() {
    String mandatoryItemsAsString;
    if (mandatoryItems.isEmpty()) {
      mandatoryItemsAsString = "no items";
    } else {
      mandatoryItemsAsString = String.join(", ", mandatoryItems);
    }

    return mandatoryItemsAsString;
  }
}
