package no.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.List;

/**
 * A Passage represents a smaller part of a story (chapter).
 * It is possible to traverse Passages with a Link.
 */
public class Passage {
  private final String title;
  private final String content;
  private final List<Link> links;

  /**
   * Constructor for a Passage.
   *
   * @param title   The title of the Passage
   * @param content The contents of the Passage (the story elements)
   */
  Passage(String title, String content) {
    this.title = title;
    this.content = content;
    this.links = new ArrayList<>();
  }

  /**
   * Returns the title of the Passage.
   *
   * @return The title of the Passage as a string
   */
  public String getTitle() {
    return title;
  }

  /**
   * Returns the content of the Passage.
   *
   * @return The contents of the Passage as a string
   */
  public String getContent() {
    return content;
  }

  /**
   * Adds a link (another choice) to the Passage.
   *
   * @param link The link to be added as part of the Passage
   * @return A boolean value representing if the link was added (false if already exists)
   */
  public boolean addLink(Link link) {

    // Checks if link already exists in List
    for (Link l : links) {
      if (l.equals(link)) {
        return false;
      }
    }

    links.add(link);
    return true;
  }

  /**
   * Returns the links associated with the Passage.
   *
   * @return A List with all the links
   */
  public List<Link> getLinks() {
    return links;
  }

  /**
   * Checks if the Passage contains any links.
   *
   * @return A boolean value that denotes if the Passage has links
   */
  public boolean hasLinks() {
    return !links.isEmpty();
  }

  /**
   * A textual representation of the Passage. Format:
   * Title
   * Content
   * Links: ##
   *
   * @return A textual representation of the Passage as a string
   */
  @Override
  public String toString() {
    return (title + "\n\n" + content + "\n\nLinks: " + links.size());
  }

  /**
   * Tests if an object contains equal values to another object.
   *
   * @param o The second object being tested
   * @return A boolean value denoting if the two objects are the same
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (this.getClass() != o.getClass()) {
      return false;
    }

    Passage passage = (Passage) o;
    return title.equals(passage.getTitle())
        && content.equals(passage.getContent())
        && links.equals(passage.getLinks());
  }

  /**
   * Returns a unique hash representing a particular instance of a Passage.
   *
   * @return An integer value that acts as a hash for the object
   */
  @Override
  public int hashCode() {
    return title.hashCode() * content.hashCode() * links.hashCode();
  }
}
