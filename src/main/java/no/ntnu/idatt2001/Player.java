package no.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that represents a player character in a game, with a name, health, score,
 * gold and inventory.
 */
public class Player {
  private final String name;
  private final List<String> inventory;
  private int health;
  private int score;
  private int gold;

  /**
   * Constructs a new player with the specified fields.
   *
   * @param builder The builder containing the properties of the player
   */
  private Player(Builder builder) {
    this.name = builder.name;
    this.health = builder.health;
    this.score = builder.score;
    this.gold = builder.gold;
    this.inventory = builder.inventory;
  }

  /**
   * Returns the player's name.
   *
   * @return The name
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the player's health.
   *
   * @return The health
   */
  public int getHealth() {
    return health;
  }

  /**
   * Set method for the health of the player.
   *
   * @param health The new health of the player
   */
  public void setHealth(int health) {
    this.health = health;
  }

  /**
   * Returns the player's score.
   *
   * @return The score
   */
  public int getScore() {
    return score;
  }

  /**
   * Set method for the score of the player.
   *
   * @param score The new score of the player
   */
  public void setScore(int score) {
    this.score = score;
  }

  /**
   * Returns the player's gold.
   *
   * @return The gold
   */
  public int getGold() {
    return gold;
  }

  /**
   * Set method for the gold of the player.
   *
   * @param gold The new gold of the player
   */
  public void setGold(int gold) {
    this.gold = gold;
  }

  /**
   * Returns the player's inventory.
   *
   * @return The inventory
   */
  public List<String> getInventory() {
    return inventory;
  }

  /**
   * Set method for the inventory of the player.
   *
   * @param inventory The new inventory of the player
   */
  public void setInventory(List<String> inventory) {
    this.inventory.clear();
    this.inventory.addAll(inventory);
  }

  /**
   * Changes the player's health by a certain amount.
   * Checks if the player's health is below zero, if this is the case the health
   * gets set to zero.
   *
   * @param health Is the change in the player's health
   */
  public void addHealth(int health) {
    this.health += health;
    if (this.health < 0) {
      this.health = 0;
    }
  }

  /**
   * Changes the player's score by a certain amount.
   *
   * @param points Is the change in the player's score
   */
  public void addScore(int points) {
    this.score += points;
  }

  /**
   * Changes the player's gold by a certain amount.
   * Checks if the player's gold is below zero, if this is the case the gold
   * gets set to zero.
   *
   * @param gold Is the change in the player's gold
   */
  public void addGold(int gold) {
    this.gold += gold;

    if (this.gold < 0) {
      this.gold = 0;
    }
  }

  /**
   * Adds an item to the player's inventory.
   *
   * @param item Is the item added
   */
  public void addToInventory(String item) {
    inventory.add(item);
  }

  /**
   * Make a deep copy of the player.
   *
   * @return A copy of the player
   */
  public Player getPlayerCopy() {
    return new Player.Builder(this.name)
        .health(this.health)
        .score(this.score)
        .gold(this.gold)
        .inventory(this.inventory)
        .build();
  }

  /**
   * Builder class for constructing objects of class Player.
   */
  public static class Builder {
    //required parameters
    private final String name;
    private final List<String> inventory = new ArrayList<>();
    //optional parameters - initialized to default values
    private int health = 100;
    private int score = 0;
    private int gold = 0;

    /**
     * Constructs a new builder with the specified name.
     *
     * @param name Is the player's name
     */
    public Builder(String name) {
      this.name = name;
    }

    /**
     * Set the health of the player.
     *
     * @param health Is the player's health
     * @return the builder
     */
    public Builder health(int health) {
      this.health = health;
      return this;
    }

    /**
     * Set the score of the player.
     *
     * @param score Is the player's score
     * @return the builder
     */
    public Builder score(int score) {
      this.score = score;
      return this;
    }

    /**
     * Set the gold of the player.
     *
     * @param gold Is the player's gold
     * @return the builder
     */
    public Builder gold(int gold) {
      this.gold = gold;
      return this;
    }

    /**
     * Set the inventory of the player.
     *
     * @param inventory Is the player's inventory
     * @return the builder
     */
    public Builder inventory(List<String> inventory) {
      this.inventory.addAll(inventory);
      return this;
    }

    /**
     * Builds a new Player object with the specified properties.
     *
     * @return the new Player object.
     */
    public Player build() {
      if (this.health < 0) {
        this.health = 0;
      }
      if (this.gold < 0) {
        this.gold = 0;
      }
      return new Player(this);
    }
  }


}

