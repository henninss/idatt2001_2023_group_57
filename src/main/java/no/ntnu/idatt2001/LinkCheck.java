package no.ntnu.idatt2001;


import java.util.ArrayList;
import java.util.List;
import no.ntnu.idatt2001.actions.Action;
import no.ntnu.idatt2001.goals.Goal;

/**
 * The LinkCheck class allows a link to lead to an alternate
 * passage depending on if a Player-object meets the specified goal.
 * A LinkCheck class has its own separate list of Action-objects.
 */
public class LinkCheck {
  private final Goal goal;
  private final String alternateReference;
  private final List<Action> actions;

  /**
   * Constructor for LinkCheck. Sets the list "actions" to a new ArrayList.
   *
   * @param alternateReference An alternate reference stored in the LinkCheck
   * @param goal               The goal that the LinkCheck checks if is fulfilled
   */
  public LinkCheck(String alternateReference, Goal goal) {
    this.alternateReference = alternateReference;
    this.goal = goal;
    actions = new ArrayList<>();
  }

  /**
   * Adds an Action-object to the list called "actions".
   *
   * @param action Is the Action-object added
   */
  public void addAction(Action action) {
    actions.add(action);
  }

  /**
   * Checks if the Goal-object is fulfilled by the
   * Player-object given as the parameter.
   *
   * @param player Is the player that gets checked for goal fulfillment
   * @return True or False depending on if the goal is fulfilled or not.
   */
  public boolean checkLinkGoal(Player player) {
    return goal.isFulfilled(player);
  }

  /**
   * Execute all the Action-objects in the list "actions"
   * on the Player-object given as the parameter.
   *
   * @param player Is the player to execute the actions upon
   */
  public void executeActions(Player player) {
    for (Action action : actions) {
      action.execute(player);
    }
  }

  /**
   * Get-method for the list "actions".
   *
   * @return actions
   */
  public List<Action> getActions() {
    return actions;
  }

  /**
   * Get-method for the alternate reference.
   *
   * @return the alternate reference
   */
  public String getAlternateReference() {
    return alternateReference;
  }
}