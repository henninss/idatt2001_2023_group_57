package no.ntnu.idatt2001;

import java.util.LinkedList;
import java.util.List;
import no.ntnu.idatt2001.goals.Goal;


/**
 * A class that represents a game instance with a player,story, a list of Goal-objects,
 * and a list of game-states.
 */
public class Game {
  private static final int GAME_STATE_STORAGE_SIZE = 100;
  private static final int UNDO_TO_FIRST_PASSAGE = 1;
  private final Player player;
  private final Story story;
  private final List<Goal> goals;
  private final LinkedList<State> gameStates;


  /**
   * Constructor for the Game-class.
   *
   * @param player The player of this game instance
   * @param story  The story that represents the game's narrative
   * @param goals  A set of goals that can be fulfilled during the game
   */
  public Game(Player player, Story story, List<Goal> goals) {
    this.player = player;
    this.story = story;
    this.goals = goals;

    gameStates = new LinkedList<>();
    gameStates.add(new State(new Player.Builder(player.getName()).health(player.getHealth())
        .score(player.getScore()).gold(player.getGold()).inventory(player.getInventory()).build(),
        null));
  }

  /**
   * Get-method for the Player-object of the game.
   *
   * @return The player
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Get-method for the Story-object of the game.
   *
   * @return The story
   */
  public Story getStory() {
    return story;
  }

  /**
   * Get-method for the list of Goal-objects in the game.
   *
   * @return the list "goals"
   */
  public List<Goal> getGoals() {
    return goals;
  }

  /**
   * Returns the opening passage of the story.
   *
   * @return the opening passage
   */
  public Passage begin() {
    return story.getOpeningPassage();
  }

  /**
   * Executes the Link-object given in the parameter on the
   * player of the Game-object. Stores the new state of the
   * player and the link in a new State-object. Sets the link's
   * reference to its original reference in case it was changed during
   * the execute step. Returns the passage that was connected to the link.
   *
   * @param link The link to execute and find the connected passage to
   * @return The connected passage to the link
   */
  public Passage go(Link link) {
    final Passage connectedPassage;

    link.executeLink(player);
    connectedPassage = story.getPassage(link);

    storeState(new Link(link.getText(), link.getReference()));

    link.setToOriginalReference();

    return connectedPassage;
  }

  /**
   * Adds a State object with the current Player and link to
   * the linkedList gameStates. If the linkedList gameStates
   * is at its maximum capacity then it removes the first state object
   * in the list before adding the new one.
   *
   * @param link Is the link that is saved in the State object.
   */
  public void storeState(Link link) {
    if (gameStates.size() >= GAME_STATE_STORAGE_SIZE) {
      gameStates.removeFirst();
    }
    gameStates.add(new State(new Player.Builder(player.getName()).health(player.getHealth())
        .score(player.getScore()).gold(player.getGold()).inventory(player.getInventory()).build(),
        link));
  }

  /**
   * Returns the previous passage, and sets the player's attributes
   * to what they were before the last link. Returns the previous
   * passage by getting it via its link. This is unless the previous
   * passage was the opening passage, in which case the method just
   * uses the .begin() method.
   *
   * @return the previous passage
   */
  public Passage undo() {
    gameStates.removeLast();
    State previousGameState = gameStates.getLast();

    player.setHealth(previousGameState.player().getHealth());
    player.setGold(previousGameState.player().getGold());
    player.setScore(previousGameState.player().getScore());
    player.setInventory(previousGameState.player().getInventory());

    if (gameStates.size() == UNDO_TO_FIRST_PASSAGE) {
      return begin();
    } else {
      return story.getPassage(previousGameState.link());
    }
  }

  /**
   * Checks if there are more than one GameState store in the linkedList "gameStates".
   *
   * @return true if there are more than one gameState, false if not
   */
  public Boolean hasOtherGameStates() {
    return gameStates.size() != 1;
  }

  /**
   * Gets the last State stored in the linkedList "gameStates".
   *
   * @return the last GameState
   */
  public State getLastGameState() {
    return gameStates.getLast();
  }
}