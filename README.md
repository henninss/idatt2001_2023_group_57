# Paths - IDATT2001 - Group 57

Welcome to group 57's final project deliverable. To clone the repository, use the clone button in the top right.

Paths is an interactive RPG experience, with features such as saving, undo, and custom goal creation. 

## Getting started

To clone the repository, use the clone button in the top right. Extract the repository to start installation.

## Installation and usage

Installing and using the program is done using Maven. Most importantly, the `mvn` executable has to be in your PATH (able to run from your command line). For further instructions, check out [Maven's installation page](https://maven.apache.org/install.html).

### After installing Maven:
1. Run `mvn -v` to confirm that you have Maven installed, and compare with Maven's installation instructions
2. Open the command line to the root folder of the repository
3. Run the program with `mvn clean javafx:run`. On Windows, you can put this command inside a .bat file that is located inside the root directory for easier access. The same is possible on unix-based systems with shell scripting.

## Authors
- Henning Sara Strandå (henninss@stud.ntnu.no)
- Erik Sandvik (erisandv@stud.ntnu.no)